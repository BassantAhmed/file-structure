package eg.edu.alexu.csd.filestructure.avl.cs18;

import eg.edu.alexu.csd.filestructure.avl.IAVLTree;
import eg.edu.alexu.csd.filestructure.avl.INode;

/**
 * Created by HP on 3/12/2018.
 */
public class AvlTree implements IAVLTree {

    private INode root ;
    private int numberOfNodes ;

    

    public void insert(Comparable key) {
        if(root == null){
            root = new Node();
            root.setValue(key);


        }else
        InsertNode(key ,(Node)root);

        numberOfNodes++;
        }

    private void rotateRight(Node main) {

        Node x = (Node) main.getLeftChild();
        Node y = (Node) x.getRightChild();
        Node p = (Node) main.getParent();
        if(p == null){
            root = x ;
        }else {
            p.setNewChild(main , x);
        }
        x.setParent(p);
        x.setRight(main);
        main.setParent(x);
        main.setLeft(y);
        if(y != null) {
            y.setParent(main);
        }
        SetNewHeight(main);
        SetNewHeight(x);
    }

    private void InsertNode(Comparable key ,Node startNode) {
        int compare = key.compareTo(startNode.getValue());
        if( compare < 0){
            if(startNode.getLeftChild() == null){
                Node newNode = new Node();
                newNode.setValue(key);
                startNode.setLeft(newNode);
                newNode.setParent(startNode);
            }
            else
            InsertNode(key , (Node) startNode.getLeftChild());
        }else if(compare > 0 ){
            if(startNode.getRightChild() == null){
                Node newNode = new Node();
                newNode.setValue(key);
                startNode.setRight(newNode);
                newNode.setParent(startNode);
            }
            else
            InsertNode(key , (Node) startNode.getRightChild());
        }
       SetNewHeight(startNode);
        if(!isBalanced(startNode)){
            Rotate(startNode);
        }
    }

    private void SetNewHeight(Node node) {
        int rightChildHeight = -1;
        int leftChildHeight = -1;
        if(node.getRightChild() != null){
            rightChildHeight = ((Node)node.getRightChild()).getHeight();
        }
        if(node.getLeftChild() != null){
            leftChildHeight = ((Node)node.getLeftChild()).getHeight();
        }
        int NodeHeight = Math.max(rightChildHeight
                ,leftChildHeight );
        node.setHeight(NodeHeight + 1);
    }

    private void DeletedBalance(Node wantedNode) {
        while (wantedNode != null){
            SetNewHeight(wantedNode);
            if(!isBalanced(wantedNode)){
                Rotate(wantedNode);
            }
            wantedNode = (Node) wantedNode.getParent();
        }
    }

    private void Rotate(Node node) {
        //TODO rotation conditions ( ll ,RR , LR , RL )
        int rightChildHeight = -1;
        int leftChildHeight = -1;
        if(node.getRightChild() != null){
            rightChildHeight = ((Node)node.getRightChild()).getHeight();
        }
        if(node.getLeftChild() != null){
            leftChildHeight = ((Node)node.getLeftChild()).getHeight();
        }
        int comparison =  leftChildHeight - rightChildHeight ;
        if(comparison > 0 ){
            Node nodeLeft = (Node) node.getLeftChild();
            rightChildHeight = -1;
            leftChildHeight = -1;
            if(nodeLeft.getRightChild() != null){
                rightChildHeight = ((Node)nodeLeft.getRightChild()).getHeight();
            }
            if(nodeLeft.getLeftChild() != null){
                leftChildHeight = ((Node)nodeLeft.getLeftChild()).getHeight();
            }
            int compare  = leftChildHeight - rightChildHeight ;
           if(compare > 0) {
               rotateRight(node);
           }else {
              rotateLeft(nodeLeft);
              rotateRight(node);
           }
        }else {
            Node nodeRight = (Node) node.getRightChild();
            rightChildHeight = -1;
            leftChildHeight = -1;
            if(nodeRight.getRightChild() != null){
                rightChildHeight = ((Node)nodeRight.getRightChild()).getHeight();
            }
            if(nodeRight.getLeftChild() != null){
                leftChildHeight = ((Node)nodeRight.getLeftChild()).getHeight();
            }
            int compare  = leftChildHeight - rightChildHeight ;
            if(compare < 0){
                rotateLeft(node);
            }else {
                rotateRight(nodeRight);
                rotateLeft(node);

            }
        }
    }

    private boolean isBalanced (Node node){
        int rightChildHeight = -1;
        int leftChildHeight = -1;
        if(node.getRightChild() != null){
            rightChildHeight = ((Node)node.getRightChild()).getHeight();
        }
        if(node.getLeftChild() != null){
            leftChildHeight = ((Node)node.getLeftChild()).getHeight();
        }
        int difference = Math.abs(rightChildHeight - leftChildHeight);
        return difference  < 2 ;
    }


    private Node searchNode (Comparable key, Node starPoint) {
        int compare = key.compareTo(starPoint.getValue());
        if (compare == 0) {
            return starPoint;
        } else if (compare < 0) {
            if (starPoint.hasLeftChild()) {
              return  searchNode(key, (Node) starPoint.getLeftChild());
            }
        }else {
                if (starPoint.hasRightChild()) {
                    return searchNode(key, (Node) starPoint.getRightChild());
                }
            }
        return null;
    }

    public boolean delete(Comparable key) {
        Node node = new Node();
        node = searchNode(key, (Node) root);
        if (node != null) {
            if(numberOfNodes == 1){
                root = null ;
                numberOfNodes -- ;
                return true;
            }
            if(!node.hasLeftChild() && !node.hasRightChild()){
                Node parentNode = (Node) node.getParent();
                if(parentNode.hasLeftChild() &&
                	parentNode.getLeftChild().getValue().equals(key)){
                		parentNode.setLeft(null);
               }else if(parentNode.hasRightChild() &&
                    parentNode.getRightChild().getValue().equals(key)) {
                        parentNode.setRight(null);
               }
            }else {
                swap(node);
            }
            numberOfNodes -- ;
            return true;
        }
        return false;
    }

    public void rotateLeft(Node n) {
        Node r = (Node) n.getRightChild();
        Node p = (Node) n.getParent();
        Node l = (Node) r.getLeftChild();
        if(p == null){
            root = r ;
        }
        else {
            p.setNewChild(n , r);
        }
        r.setParent(p);
        r.setLeft(n);
        n.setParent(r);
        n.setRight(l);
        if(l != null){
            l.setParent(n);
        }
        SetNewHeight(r);
        SetNewHeight(n);
    }

        public int size() {
        return numberOfNodes;
    }

    private void swap (Node node) {
        Node wantedNode = new Node() ;
        if(node.hasLeftChild()) {
            wantedNode = (Node) node.getLeftChild();
            while(wantedNode.hasRightChild()){
                wantedNode = (Node) wantedNode.getRightChild();
            }
            ((Node)wantedNode.getParent()).setNewChild(wantedNode , null);
        }else if(node.hasRightChild()) {
            wantedNode = (Node) node.getRightChild();
            while(wantedNode.hasLeftChild()){
                wantedNode = (Node) wantedNode.getLeftChild();
            }
            ((Node)wantedNode.getParent()).setNewChild(wantedNode , null);
        }
        node.setValue(wantedNode.getValue());
        DeletedBalance((Node) wantedNode.getParent());
    }

    public boolean search(Comparable key) {
      if(searchNode(key , (Node) root) == null){
          return false;
      }
          return true;
    }

    public int height() {
        if(root == null){
            return -1 ;
        }
        return ((Node)root).getHeight() +1 ;
    }

    public INode getTree() {
        return root;
    }
}
