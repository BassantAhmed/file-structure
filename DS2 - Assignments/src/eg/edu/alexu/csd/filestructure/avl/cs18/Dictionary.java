package eg.edu.alexu.csd.filestructure.avl.cs18;

import eg.edu.alexu.csd.filestructure.avl.IDictionary;

import java.io.*;

/**
 * Created by HP on 3/12/2018.
 */
public class Dictionary implements IDictionary {
    private AvlTree tree;

    public Dictionary() {
        tree = new AvlTree();
    }


    public void load(File file) {
        String line = null;

        try {
            FileReader fileReader = new FileReader(file);

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null) {
                tree.insert(line);
            }

            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Unable to open file '" + file.getAbsolutePath() + "'");
        } catch (IOException ex) {
            System.out.println("Error reading file '" + file.getAbsolutePath() + "'");

        }
    }


    public boolean insert(String word) {
        if(this.exists(word)) {
            return false;
        }
        tree.insert(word);
        return true;
    }


    public boolean exists(String word) {
        return tree.search(word);
    }


    public boolean delete(String word) {
        if(this.exists(word)) {
            tree.delete(word);
            return true;
        }
        return false;
    }

    public int size() {
        return tree.size();
    }


    public int height() {
        return tree.height();
    }
}
