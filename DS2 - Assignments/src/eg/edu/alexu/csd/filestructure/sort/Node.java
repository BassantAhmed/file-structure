/**
 *
 */
package eg.edu.alexu.csd.filestructure.sort;

 /** class node.
 * @author Personal
 * @param <T>
 *
 */

public class Node<T extends Comparable<T>> implements INode<T> {

	/** variable.
	 * parent of the node
	 *
	 */
	private INode parent;
	/** variable.
	 * leftChild of the node
	 *
	 */
    private INode leftChild;
    /** variable.
	 * rightChild of the node
	 *
	 */
    private INode rightChild;
    /** variable.
	 * value of the node
	 *
	 */
    private Comparable nodeValue;

    /** constructor.
     * @author Personal
     *
     */
    public Node() {
    	parent = null;
    }
    /** gets the left child of the current.
	* element/node in the heap tree
	* @return INode wrapper to the left child of the current element/node
	*/
	@SuppressWarnings("unchecked")
	public final INode<T> getLeftChild() {
        return leftChild;
    }

	/** sets the left child of the current.
	* element/node in the heap tree
	* @param nodeLeftChild node left child
	*/

     public final void setLeftChild(final INode<T> nodeLeftChild) {
        this.leftChild = nodeLeftChild;
    }
    /** Returns the right child of the current.
    * element/node in the heap tree
    * @return INode wrapper to the right child of the current element/node
    */

	public final INode<T> getRightChild() {

        return rightChild;
    }

	/** sets the right child of the current.
	* element/node in the heap tree
	* @param nodeRightChild node right child.
	*/

    public final void setRightChild(final INode<T> nodeRightChild) {

      this.rightChild = nodeRightChild;
    }
    /** Returns the parent node of the current.
    * element/node in the heap tree
    * @return INode wrapper to the parent of the current element/node
    */

	public final INode<T> getParent() {

        return parent;
    }
	/** sets the parent of the current.
	* element/node in the heap tree
	* @param nodeParent node parent.
	*/
    public final void setParent(final INode<T> nodeParent) {

      this.parent = nodeParent;
    }

    /** Set/Get the value.
    * of the current node
    * @return Value of the current node
    */
    public final T getValue() {
        return (T) nodeValue;
    }

    /** sets the value of the current.
	* element/node in the heap tree
	* @param value node value.
	*/

    public final void setValue(final Comparable value) {
        nodeValue = value;

    }

}
